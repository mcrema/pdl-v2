package pdl.backend;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ImageControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ImageController imageController;

    @Autowired
    private ObjectMapper mapper;

    private ImageDao imageDao;
    private final List<String> acceptedTestImages = new ArrayList<>();
    private Path oldFolderPath;

    @BeforeAll
    void setUp() {
        oldFolderPath = (Path) ReflectionTestUtils.getField(BackendApplication.class, "IMAGE_FOLDER_PATH");
        ReflectionTestUtils.setField(BackendApplication.class, "IMAGE_FOLDER_PATH", Path.of("testImages"));
        this.imageDao = new ImageDao();
        ReflectionTestUtils.setField(this.imageController, "imageDao", this.imageDao);
        acceptedTestImages.add("test0.jpg");
        acceptedTestImages.add("test_a.jpg");
        acceptedTestImages.add("test_a_b.jpg");
        acceptedTestImages.add("test_b.jpg");
    }

    @Test
    @Order(-1)
    void imageDaoShouldOnlyFindValidImages() {
        this.imageDao.retrieveAll().stream()
                .map(Image::getName)
                .forEach(s -> Assertions.assertTrue(acceptedTestImages.contains(s)));
    }

    @Test
    @Order(0)
    void getImageListShouldReturnCorrectImageList() throws Exception {
        mockMvc.perform(get("/images")).andDo(mvcResult -> {
            String body = mvcResult.getResponse().getContentAsString();
            JsonNode array = mapper.readTree(body);
            array.forEach(jsonObj -> {
                String imgName = jsonObj.get("name").asText();
                Assertions.assertTrue(acceptedTestImages.contains(imgName));
            });
        });
    }

    @Test
    @Order(1)
    public void getImageListShouldReturnSuccess() throws Exception {
        mockMvc.perform(get("/images")).andExpect(status().isOk());
    }

    @Test
    @Order(2)
    public void getImageShouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/images/4")).andExpect(status().isNotFound());
    }

    @Test
    @Order(3)
    public void getImageShouldReturnSuccess() throws Exception {
        mockMvc.perform(get("/images/0")).andExpect(status().isOk());
    }

    @Test
    @Order(4)
    public void deleteImageShouldReturnBadRequest() throws Exception {
        mockMvc.perform(delete("/images/-1")).andExpect(status().isBadRequest());
    }

    @Test
    @Order(5)
    public void deleteImageShouldReturnNotFound() throws Exception {
        mockMvc.perform(delete("/images/5")).andExpect(status().isNotFound());
    }

    @Test
    @Order(6)
    public void deleteImageShouldReturnSuccess() throws Exception {
        mockMvc.perform(delete("/images/0")).andExpect(status().isOk());
    }

    @Test
    @Order(7)
    public void createImageShouldReturnSuccess() throws Exception {
        ClassPathResource testImg = new ClassPathResource("test.jpg");
        MockMultipartFile mpf = new MockMultipartFile("file", "test.jpg", MediaType.IMAGE_JPEG_VALUE, testImg.getInputStream());
        mockMvc.perform(multipart("/images").file(mpf)).andExpect(status().isCreated());
    }

    @Test
    @Order(8)
    public void createImageShouldReturnUnsupportedMediaType() throws Exception {
        MockMultipartFile mpf = new MockMultipartFile("file", new byte[]{});
        mockMvc.perform(multipart("/images").file(mpf)).andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @Order(9)
    public void testAddAlgorithmToAlgoList() throws Exception {
        imageController.addAlgorithmToAlgoList(0, "[{\"name\":\"brightness\", \"parameters\": [300] }, {\"name\":\"jsp\", \"parameters\": [300, 400] }]");
    }

    @AfterAll
    void finishUp() {
        ReflectionTestUtils.setField(BackendApplication.class, "IMAGE_FOLDER_PATH", oldFolderPath);
    }
}
