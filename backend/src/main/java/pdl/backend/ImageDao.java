package pdl.backend;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static pdl.backend.BackendApplication.IMAGE_FOLDER_PATH;

@Repository
public class ImageDao implements Dao<Image> {
    public static final MediaType[] ACCEPTED_TYPES = {MediaType.IMAGE_JPEG, BackendMediaTypes.IMAGE_TIFF};
    private final Map<Long, Image> images = new HashMap<>();
    private int nextImageId = 0;

    public ImageDao() {
        try (Stream<Path> walk = Files.walk(IMAGE_FOLDER_PATH)) {
            walk.filter(Files::isRegularFile).forEach(p -> {
                try {
                    String contentType = Files.probeContentType(p);
                    Optional<MediaType> optAcceptedMediaType = Arrays.stream(ImageDao.ACCEPTED_TYPES)
                            .filter(t -> t.toString().equals(contentType))
                            .findFirst();
                    if (optAcceptedMediaType.isPresent()) {
                        String fileName = p.getFileName().toString();
                        byte[] fileContent = Files.readAllBytes(p);
                        Image img = new Image(this.getNextImageId(), fileName, optAcceptedMediaType.get(), fileContent);
                        this.create(img);
                    }
                } catch (IOException e) {
                    System.err.println("Error adding image! " + e);
                }
            });
        } catch (IOException e) {
            System.err.println("Error walking the images path! " + e);
        }
    }

    @Override
    public Optional<Image> retrieve(long id) {
        return Optional.ofNullable(this.images.get(id));
    }

    @Override
    public List<Image> retrieveAll() {
        return new ArrayList<>(this.images.values());
    }

    @Override
    public void create(Image img) {
        this.images.put(img.getId(), img);
        this.nextImageId++;
    }

    @Override
    public void update(Image img, String[] params) {
        // Not used
    }

    @Override
    public void delete(Image img) {
        this.images.remove(img.getId());
    }

    public int getNextImageId() {
        return this.nextImageId;
    }
}
