package pdl.backend;

import org.springframework.http.MediaType;

public class BackendMediaTypes {
    public static final MediaType IMAGE_TIFF = new MediaType("image", "tiff");
}
