package pdl.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmParamsInfo {

    private List<SingleParamsInfo> paramsList;
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public AlgorithmParamsInfo() {
        this.paramsList = new ArrayList<>();
    }

    public AlgorithmParamsInfo(String name, String jsType, int boundMin, int boundMax){
        this.paramsList = new ArrayList<>();
        SingleParamsInfo param = new SingleParamsInfo(name, jsType, boundMin, boundMax);
        this.paramsList.add(param);
    }

    public AlgorithmParamsInfo(String name, String jsType, int var1, int var2, int var3){
        this.paramsList = new ArrayList<>();
        SingleParamsInfo param = new SingleParamsInfo(name, jsType, var1, var2, var3);
        this.paramsList.add(param);
    }

    @Override
    public String toString() {
        String msg = "[";
        for (SingleParamsInfo info : this.paramsList) {
            msg += "'" + info.getName() + "', "
                   + "'" + info.getJsType() + "', "
                   + info.getBoundString()
            ;
        }
        msg += "]";
        return msg;
    }

    public ArrayNode toArrayNode() {
        ArrayNode arrayNode = MAPPER.createArrayNode();
        this.paramsList.forEach(p -> {
            ObjectNode paramObjectNode = MAPPER.createObjectNode();
            paramObjectNode.put("name", p.getName()).put("type",p.getJsType());
            ArrayNode boundsArrayNode = MAPPER.createArrayNode();
            for (int i : p.intTab) {
                boundsArrayNode.add(i);
            }
            paramObjectNode.set("bounds",boundsArrayNode);
            arrayNode.add(paramObjectNode);
        });
        return arrayNode;
    }

    // ex : [['numberA', 'number', [25, 666]]
    private class SingleParamsInfo { //make it more dynamic with a type for example (ex: type "intBounded" is the current state)
        private String name;
        private String jsType;
        private int[] intTab;

        public SingleParamsInfo(String name, String jsType, int boundMin, int boundMax) {
            int[] newBound = {boundMin, boundMax};
            this.intTab = newBound;
            this.name = name;
            this.jsType = jsType;
        }

        public SingleParamsInfo(String name, String jsType, int var1, int var2, int var3) {
            int[] newBound = {var1, var2, var3};
            this.intTab = newBound;
            this.name = name;
            this.jsType = jsType;
        }

        public String getName() {
            return name;
        }

        public String getJsType() {
            return jsType;
        }

        public String getBoundString(){
            String msg = "[";
            for (int i = 0; i < this.intTab.length; i++) {
                msg += this.intTab[i];
                if(i != this.intTab.length -1){
                    msg += ", ";       
                }
            }
            msg += "]";
            return msg;
        }
    }
}
