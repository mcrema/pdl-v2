package pdl.backend;

import io.scif.*;
import io.scif.formats.JPEGFormat;
import io.scif.formats.TIFFFormat;
import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.scijava.Context;
import org.scijava.io.location.BytesLocation;
import org.springframework.http.MediaType;

import java.io.IOException;

public class ImageConverter {

    public static SCIFIOImgPlus<UnsignedByteType> imageFromBytes(MediaType type, byte[] data) throws FormatException, IOException {
        final ImgOpener imgOpener = new ImgOpener();
        final Context c = imgOpener.getContext();
        final SCIFIO scifio = new SCIFIO(c);
        Format format;
        if (type.equals(MediaType.IMAGE_JPEG)) {
            format = scifio.format().getFormatFromClass(JPEGFormat.class);
        } else if (type.equals(BackendMediaTypes.IMAGE_TIFF)) {
            format = scifio.format().getFormatFromClass(TIFFFormat.class);
        } else {
            throw new IllegalArgumentException("type is neither image/jpeg nor image/tiff");
        }
        final Reader r = format.createReader();
        final BytesLocation location = new BytesLocation(data);
        r.setSource(location);
        final ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
        SCIFIOImgPlus<UnsignedByteType> img = imgOpener.openImgs(r, factory, null).get(0);
        c.dispose();
        return img;
    }

    public static byte[] imageToBytes(SCIFIOImgPlus<UnsignedByteType> img) throws FormatException, IOException {
        final ImgSaver imgSaver = new ImgSaver();
        final Context c = imgSaver.getContext();
        final Format format = img.getMetadata().getFormat();
        final Writer w = format.createWriter();
        final BytesLocation saveLocation = new BytesLocation(10);
        w.setMetadata(img.getMetadata());
        w.setDest(saveLocation);
        imgSaver.saveImg(w, img);
        byte[] imageInByte = saveLocation.getByteBank().toByteArray();
        c.dispose();
        return imageInByte;
    }
}