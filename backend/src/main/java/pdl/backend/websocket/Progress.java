package pdl.backend.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.java_websocket.WebSocket;

import java.util.UUID;

public class Progress {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private final String id;
    private int percentage;
    private String error;
    private WebSocket subscriberConn;

    public Progress() {
        this(UUID.randomUUID().toString());
    }
    public Progress(String id) {
        this(id, null);
    }

    public Progress(WebSocket subscriberConn) {
        this(UUID.randomUUID().toString(), subscriberConn);
    }

    public Progress(String id, WebSocket subscriberConn) {
        this.id = id;
        this.percentage = 0;
        this.error = "";
        this.subscriberConn = subscriberConn;
    }

    public String getId() {
        return id;
    }

    public String getError() {
        return error;
    }

    public void setSubscriberConn(WebSocket subscriberConn) {
        this.subscriberConn = subscriberConn;
    }

    public void updatePercentage(int percentage) {
        this.percentage = percentage;
        sendUpdateToWebSocket();
    }

    public void updateError(String error) {
        this.error = error;
        sendUpdateToWebSocket();
    }

    protected void sendUpdateToWebSocket() {
        if (subscriberConn != null) {
            ObjectNode objectNode = MAPPER.createObjectNode();
            objectNode.put("id", id).put("percentage", percentage).put("error", error);
            subscriberConn.send(objectNode.toString());
        } else {
            System.err.println("Warning: no subscriber for progress: " + this);
        }
    }

    @Override
    public String toString() {
        if(this.subscriberConn != null)
            return "Progress id " + id + ", percentage " + percentage + "%, subscriber IP " + subscriberConn.getRemoteSocketAddress();
        return "Progress id " + id + ", percentage " + percentage + '%';
    }
}
