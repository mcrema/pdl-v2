package pdl.backend.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class WSServer extends WebSocketServer {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private final List<Progress> progressList = new ArrayList<>();

    public WSServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake clientHandshake) {
        System.out.println("Opened " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Closed " + conn.getRemoteSocketAddress() + " with exit code " + code + ".\n" +
                           "Reason: " + reason);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println("Received message from " + conn.getRemoteSocketAddress() + ": " + message);
        try {
            String progressId = MAPPER.readTree(message).get("progressId").asText();
            progressList.stream()
                    .filter(p -> p.getId().equals(progressId))
                    .forEach(p -> {
                        p.setSubscriberConn(conn);
                        p.sendUpdateToWebSocket();
                    });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("An error has occurred on connection " + conn.getRemoteSocketAddress() + ": " + ex);
    }

    @Override
    public void onStart() {
        System.out.println("WebSocket server started successfully.");
    }

    public void addProgress(Progress progress) {
        this.progressList.add(progress);
    }

    public void removeProgress(Progress progress) {
        this.progressList.remove(progress);
    }
}
