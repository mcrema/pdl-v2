package pdl.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.scif.FormatException;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pdl.backend.websocket.Progress;
import pdl.imageProcessing.Color;
import pdl.imageProcessing.Convolution;
import pdl.imageProcessing.GrayLevelProcessing;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static pdl.backend.AlgorithmInfo.*;

@RestController
public class ImageController {
    private final ObjectMapper mapper;
    private final ImageDao imageDao;

    @Autowired
    public ImageController(ImageDao imageDao, ObjectMapper mapper) {
        this.imageDao = imageDao;
        this.mapper = mapper;
    }

    @GetMapping(value = "/images/{id}", params = "!algorithm", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<?> getImage(@PathVariable("id") long id) {
        System.out.println("> getImage : id = " + id);
        Optional<Image> optImg = imageDao.retrieve(id);
        if (optImg.isPresent()) {
            return new ResponseEntity<>(optImg.get().getOriginalData(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/images/{id}/processed", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<?> getProcessedImage(@PathVariable("id") long id) {
        System.out.println("> getProcessedImage : id = " + id);
        Optional<Image> optImg = imageDao.retrieve(id);
        if (optImg.isPresent() && optImg.get().getProcessedData() != null) {
            return new ResponseEntity<>(optImg.get().getProcessedData(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/images/{id}")
    public ResponseEntity<?> deleteImage(@PathVariable("id") long id) {
        if (id < 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<Image> optImg = imageDao.retrieve(id);
        if (optImg.isPresent()) {
            imageDao.delete(optImg.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/images")
    public ResponseEntity<?> addImage(@RequestParam("file") MultipartFile file) {
        Optional<MediaType> optAcceptedMediaType = Arrays.stream(ImageDao.ACCEPTED_TYPES)
                .filter(t -> t.toString().equals(file.getContentType()))
                .findFirst();
        if (optAcceptedMediaType.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
        try {
            Image img = new Image(imageDao.getNextImageId(), file.getOriginalFilename(), optAcceptedMediaType.get(), file.getBytes());
            imageDao.create(img);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/images", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayNode getImageList() {
        ArrayNode nodes = mapper.createArrayNode();
        //change the stream behind
        imageDao.retrieveAll().forEach(img -> nodes.addObject()
                .put("id", img.getId())
                .put("name", img.getName())
                .put("type", img.getType().toString())
                .put("size", img.getOriginalData().length)
        );
        return nodes;
    }

    @GetMapping(value = "/algorithms", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayNode getAlgorithmsInfoList() {
        ArrayNode nodes = mapper.createArrayNode();
        AlgorithmInfo.stream().forEach(info -> nodes.addObject()
                .put("name", info.getName())
                .put("description", info.getDescription())
                .set("parameters", info.getParams().toArrayNode())
        );
        return nodes;
    }

    @PostMapping(value = "/images/{id}/process/add") //todo change the url
    public ResponseEntity<?> addAlgorithmToAlgoList(@PathVariable("id") long id, @RequestBody String requestBody) {
        //get input image
        SCIFIOImgPlus<UnsignedByteType> input = getInputSCIFIOImg(id);
        if (input == null) {
            ObjectNode json = getJsonMessage("A server error has occurred while trying to get the input image");
            return new ResponseEntity<>(json, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<String> algoNames = new LinkedList<>();
        List<List<Number>> algoParams = new LinkedList<>();
        try {
            // Read ObjectNode array
            ObjectNode[] objectNodes = mapper.readValue(requestBody, ObjectNode[].class);
            for (ObjectNode objectNode : objectNodes) {
                // Read all algorithm data
                String algoName = objectNode.get("name").asText();
                ArrayNode algoParamsArrNode = objectNode.withArray("parameters");

                List<Number> algoParamsList = new LinkedList<>();
                System.out.println("> " + algoName);
                algoParamsArrNode.forEach(n -> {
                    algoParamsList.add(n.numberValue());
                    System.out.println("- " + n);
                });

                // Add to the algorithm list
                algoNames.add(algoName);
                algoParams.add(algoParamsList);
            }
        } catch (Exception e) { //todo change the exception
            e.printStackTrace();
            return new ResponseEntity<>(getJsonMessage("could not parse JSON"), HttpStatus.BAD_REQUEST);
        }

        Progress progress = new Progress();
        BackendApplication.WS_SERVER.addProgress(progress);
        //apply the algorithm
        runAlgorithmOnSeparateThread(id, input, algoNames, algoParams, progress);

        ObjectNode node = mapper.createObjectNode().put("progressId", progress.getId());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(node);
    }

    private void runAlgorithmOnSeparateThread(long id, SCIFIOImgPlus<UnsignedByteType> input, List<String> algoNames, List<List<Number>> algoParams, Progress progress) {
        new Thread(() -> {
            progress.updatePercentage(0);
            SCIFIOImgPlus<UnsignedByteType> output;
            byte[] imageBytes;
            try {
                output = applyAlgoFromAlgoList(input, algoNames, algoParams, progress);
                progress.updatePercentage(90);
                imageBytes = ImageConverter.imageToBytes(output);
                imageDao.retrieve(id).ifPresent(img -> img.setProcessedData(imageBytes));
                progress.updatePercentage(100);
            } catch (Exception e) {
                e.printStackTrace();
                progress.updateError("An error has occurred while applying algorithm!");
            } finally {
                BackendApplication.WS_SERVER.removeProgress(progress);
            }
        }).start();
    }

    /**
     * To apply algorithm in the list
     *
     * @param input      input image
     * @param algoNames  names of the algorithms to be applied
     * @param algoParams parameters of the algorithms
     */
    public SCIFIOImgPlus<UnsignedByteType> applyAlgoFromAlgoList(SCIFIOImgPlus<UnsignedByteType> input,
                                                                 List<String> algoNames,
                                                                 List<List<Number>> algoParams,
                                                                 Progress progress) {
        //check that lists' length are coherent
        if (algoParams.size() != algoNames.size()) {
            throw new IllegalArgumentException("Algorithm names size does not match algorithm param size!");
        }

        //apply algorithm in the list
        for (int i = 0; i < algoNames.size(); i++) {
            String algoName = algoNames.get(i);
            List<Number> paramList = algoParams.get(i);

            //todo it's a temporay solution because I dont found one with switch statement
            if (algoName.equals(BRIGHTNESS.getName())) {
                int brightnessParam = paramList.get(0).intValue();
                GrayLevelProcessing.modifyBrightnessColor(input, brightnessParam);

            } else if (algoName.equals(COLOR.getName())) {
                int colorValue = paramList.get(0).intValue();
                Color.filterColorHSVconvertion(input, colorValue);

            } else if (algoName.equals(GAUSS.getName())) {
                SCIFIOImgPlus<UnsignedByteType> output = input.copy();
                int filterSize = paramList.get(0).intValue();
                Convolution.gaussConvolution(input, output, filterSize);
                input = output;

            } else if (algoName.equals(HISTOGRAM_S.getName())) {
                GrayLevelProcessing.histogramEqualizationForSLayer(input);

            } else if (algoName.equals(HISTOGRAM_V.getName())) {
                GrayLevelProcessing.histogramEqualizationForVLayer(input);

            } else if (algoName.equals(MEAN.getName())) {
                SCIFIOImgPlus<UnsignedByteType> output = input.copy();
                int filterSize = paramList.get(0).intValue();
                Convolution.meanConvolution(input, output, filterSize);
                input = output;

            } else if (algoName.equals(OUTLINE.getName())) {
                SCIFIOImgPlus<UnsignedByteType> output = input.copy();
                Convolution.outlineDetectionFilter(input, output);
                input = output;

            } else if (algoName.equals(GREY.getName())) {
                Color.fromColorToGrey(input);

            } else if (algoName.equals(NEGATIF.getName())){
                Color.negatifFilter(input);
            }
            progress.updatePercentage((90 * (i + 1) / algoNames.size()));
        }

        return input;
    }

    /**
     * To set input image
     *
     * @param id id of the required image
     * @return image usable for filters
     */
    public SCIFIOImgPlus<UnsignedByteType> getInputSCIFIOImg(long id) {
        //get the correct input image
        Optional<Image> optInputImg = imageDao.retrieve(id);
        if (optInputImg.isEmpty()) {
            return null;
        }
        Image inputImg = optInputImg.get();

        //convert byte from server to imglib image format
        SCIFIOImgPlus<UnsignedByteType> input = null;
        try {
            input = ImageConverter.imageFromBytes(inputImg.getType(), inputImg.getOriginalData());
        } catch (FormatException | IOException e) {
            e.printStackTrace();
        }

        System.out.println("> getInputSCIFIOImg : seems ok ? = " + (input != null));
        return input;
    }

    private ObjectNode getJsonMessage(String s) {
        return mapper.createObjectNode().put("message", s);
    }
}
