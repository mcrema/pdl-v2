package pdl.backend;

import java.util.stream.Stream;

public enum AlgorithmInfo {
    BRIGHTNESS("brightness", "modfiy image's brigthness \n gain = [-255, 255]",
            new AlgorithmParamsInfo("gain", "number", -255, 255)),

    HISTOGRAM_S("histogramS", "equalize saturation's histogram -- no param",
            new AlgorithmParamsInfo()),

    HISTOGRAM_V("histogramV", "equalize brightness's histogram -- no param",
            new AlgorithmParamsInfo()),

    COLOR("color", "apply a colored filter -- hue = [0,359]",
            new AlgorithmParamsInfo("hue", "number", 0, 359)),

    MEAN("mean", "apply a mean filter -- filter size : number equals to 3, 5 or 7",
            new AlgorithmParamsInfo("filter size", "number_select_list", 3, 5, 7)),

    GAUSS("gauss", "apply a gauss filter -- filter size : number equals to 3, 5 or 7",
            new AlgorithmParamsInfo("filter size", "number_select_list", 3, 5, 7)),

    OUTLINE("outline", "detect border from shapes -- no param\"",
            new AlgorithmParamsInfo()),

    NEGATIF("negatif", "Make the image in negatif colors -- no param\"",
            new AlgorithmParamsInfo()),

    GREY("grey", "switch the image to a grey value version\"",
            new AlgorithmParamsInfo()),
    ;

    private final String name;
    private final String description;
    private final AlgorithmParamsInfo params;

    AlgorithmInfo(String name, String description, AlgorithmParamsInfo params) {
        this.name = name;
        this.description = description;
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public AlgorithmParamsInfo getParams() {
        return params;
    }

    public static Stream<AlgorithmInfo> stream() {
        return Stream.of(AlgorithmInfo.values());
    }
}
