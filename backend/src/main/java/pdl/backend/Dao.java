package pdl.backend;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    void create(T t);

    Optional<T> retrieve(long id);

    List<T> retrieveAll();

    void update(T t, String[] params);

    void delete(T t);
}
