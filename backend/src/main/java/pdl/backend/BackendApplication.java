package pdl.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pdl.backend.websocket.WSServer;

import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;

@SpringBootApplication
public class BackendApplication {
    public static Path IMAGE_FOLDER_PATH = Path.of("images");
    public static final WSServer WS_SERVER = new WSServer(new InetSocketAddress("localhost", 8887));

    public static void main(String[] args) {
        if (!Files.isDirectory(IMAGE_FOLDER_PATH)) {
            System.err.println("\"" + IMAGE_FOLDER_PATH + "\" was not found, or is not a folder! " +
                               "The server will be stopped.");
            System.exit(1);
        }
        WS_SERVER.start();
        SpringApplication.run(BackendApplication.class, args);
    }
}
