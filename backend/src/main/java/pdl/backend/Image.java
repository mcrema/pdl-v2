package pdl.backend;

import org.springframework.http.MediaType;

public class Image {
    private final long id;
    private final MediaType type;
    private final byte[] originalData;
    private String name;
    private byte[] processedData;

    public Image(long id, String name, MediaType type, byte[] originalData) {
        this(id, name, type, originalData, null);
    }

    public Image(long id, String name, MediaType type, byte[] originalData, byte[] processedData) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.originalData = originalData;
        this.processedData = processedData;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaType getType() {
        return type;
    }

    public byte[] getOriginalData() {
        return originalData;
    }

    public byte[] getProcessedData() {
        return processedData;
    }

    public void setProcessedData(byte[] processedData) {
        this.processedData = processedData;
    }

    @Override
    public String toString() {
        return "id = " + id + ", name = \"" + name + "\"";
    }
}
