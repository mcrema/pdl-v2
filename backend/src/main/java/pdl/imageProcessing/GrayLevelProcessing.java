package pdl.imageProcessing;

import net.imglib2.Cursor;
import net.imglib2.img.Img;
import net.imglib2.loops.LoopBuilder;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;
import java.util.Arrays;

public class GrayLevelProcessing {

    /**
     * To modify brigthness of a colored image according to delta value
     * the algorithm use LoopBuilder.
     *
     * @param img   image which will be modify
     * @param delta value of the luminosity modification
     */
    public static void modifyBrightnessColor(Img<UnsignedByteType> img, int delta) {
        //check params
        if (delta < -255 || delta > 255) {
            throw new IllegalArgumentException("Parameter value is not in correct bound [-255, 255]");
        }

        final Cursor<UnsignedByteType> cursor = img.cursor();
        while (cursor.hasNext()) {
            cursor.fwd();
            UnsignedByteType val = cursor.get();
            int newLumValue = val.get() + delta;
            if (newLumValue > 255) {
                val.set(255);
            } else val.set(Math.max(newLumValue, 0));
        }
    }

    /**
     * To equalize the image's histogram for S layer from HSV values
     *
     * @param img concerned image
     */
    public static void histogramEqualizationForSLayer(Img<UnsignedByteType> img) {
        int n = (int) img.size();

        //compute histogram
        float[] histogram = computeHistogramForSLayer(img);

        //compute cumulative histogram
        float[] cumulHistogram = computeCumulativeHistogram(histogram);

        //set new image's values
        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(img, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(img, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    float[] originalHSV = new float[3];
                    int[] rgb = new int[3];

                    // get the original h, s and v value
                    Color.rgbToHsv(r.get(), g.get(), b.get(), originalHSV);

                    //compute new S value using cumulative histogram as a LUT
                    int roundedSValue = Math.round( originalHSV[1] * 255);
                    float newSvalue = (cumulHistogram[roundedSValue] * 255 ) / n;
                    float newSvalue_bounded = newSvalue / 255 ;

                    //compute the new rgb values
                    Color.hsvToRgb(originalHSV[0], newSvalue_bounded, originalHSV[2], rgb);

                    r.set(rgb[0]);
                    g.set(rgb[1]);
                    b.set(rgb[2]);
                }
        );
    }

    /**
     * To initialize an histogram with the S layer from HSV
     *
     * @param img histogram's image
     * @return an int array which represents the histogram
     */
    private static float[] computeHistogramForSLayer(Img<UnsignedByteType> img) {
        //compute lookup table
        float[] histogram = new float[256]; //table's values are from 0 to 255, for grey values
        Arrays.fill(histogram, 0); //to initialize all value to 0

        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(img, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(img, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    float[] originalHSV = new float[3];
                    int[] rgb = new int[3];

                    // get the original h, s and v value
                    Color.rgbToHsv(r.get(), g.get(), b.get(), originalHSV);

                    int computedSvalue = Math.round(originalHSV[1] * 255);
                    histogram[computedSvalue]++;
                }
        );

        return histogram;
    }

    /**
     * To balance the image's histogram for V layer from HSV values
     *
     * @param img concerned image
     */
    public static void histogramEqualizationForVLayer(Img<UnsignedByteType> img) {
        int n = (int) img.size();

        //compute histogram
        float[] histogram = computeHistogramForVLayer(img);

        //compute cumulative histogram
        float[] cumulHistogram = computeCumulativeHistogram(histogram);

        //set new image's values
        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(img, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(img, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    float[] originalHSV = new float[3];
                    int[] rgb = new int[3];

                    // get the original h, s and v value
                    Color.rgbToHsv(r.get(), g.get(), b.get(), originalHSV);

                    //compute new S value using cumulative histogram as a LUT
                    int roundedVValue = Math.round( originalHSV[2] * 255 ); // --- check here if still got a problem
                    float newVValue = (cumulHistogram[roundedVValue] * 255 ) / n;
                    float newVValue_bounded = newVValue / 255;

                    //compute the new rgb values
                    Color.hsvToRgb(originalHSV[0], originalHSV[1], newVValue_bounded, rgb);

                    r.set(rgb[0]);
                    g.set(rgb[1]);
                    b.set(rgb[2]);
                }
        );
    }

    /**
     * To initialize an histogram with the V layer from HSV
     *
     * @param img histogram's image
     * @return an int array which represents the histogram
     */
    private static float[] computeHistogramForVLayer(Img<UnsignedByteType> img) {
        //compute lookup table
        float[] histogram = new float[256]; //table's values are from 0 to 255, for grey values
        Arrays.fill(histogram, 0); //to initialize all value to 0

        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(img, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(img, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(img, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    float[] originalHSV = new float[3];
                    int[] rgb = new int[3];

                    // get the original h, s and v value
                    Color.rgbToHsv(r.get(), g.get(), b.get(), originalHSV);

                    int computedVvalue = Math.round(originalHSV[2] * 255);
                    histogram[computedVvalue]++;
                }
        );

        return histogram;
    }

    /**
     * To compute the cumulative histogram
     *
     * @param histogram image's histogram
     * @return the cumulative histogram
     */
    private static float[] computeCumulativeHistogram(float[] histogram) {
        float[] cumulHistogram = new float[256];

        for (int i = 0; i < 256; i++) {
            if (i == 0) {
                cumulHistogram[0] = histogram[0];
            } else {
                cumulHistogram[i] = cumulHistogram[i - 1] + histogram[i];
            }
        }

        return cumulHistogram;
    }
}
