package pdl.imageProcessing;

import io.scif.img.ImgIOException;
import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import net.imglib2.*;
import net.imglib2.algorithm.neighborhood.Neighborhood;
import net.imglib2.algorithm.neighborhood.RectangleShape;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.util.Intervals;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.NoSuchElementException;

public class Convolution {

    /**
     * To apply a Gauss' mean filter
     *
     * @param input  input image
     * @param output output image
     * @param size   filter size
     */
    public static void gaussConvolution(Img<UnsignedByteType> input, Img<UnsignedByteType> output, int size) {
        //check parameter value
        if(size != 3 && size != 5 && size != 7){
            throw new IllegalArgumentException("Filter size is not correct");
        }

        int[][] kernel3 = {
                {1, 2, 1},
                {2, 4, 2},
                {1, 2, 1}
        };
        int[][] kernel5 = {
                {1, 4, 7, 4, 1},
                {4, 16, 26, 16, 4},
                {7, 26, 41, 26, 7},
                {4, 16, 26, 16, 4},
                {1, 4, 7, 4, 1}
        };
        int[][] kernel7 = {
                {0, 0, 1, 2, 1, 0, 0},
                {0, 3, 13, 22, 13, 3, 0},
                {1, 13, 59, 97, 59, 13, 1},
                {2, 22, 97, 159, 97, 22, 2},
                {1, 13, 59, 97, 59, 13, 1},
                {0, 3, 13, 22, 13, 3, 0},
                {0, 0, 1, 2, 1, 0, 0}
        };
        switch (size) {
            case 3: {
                convol_pierre(input, output, kernel3);
                break;
            }
            case 5: {
                convol_pierre(input, output, kernel5);
                break;
            }
            case 7: {
                convol_pierre(input, output, kernel7);
                break;
            }
            default:
                break;
        }
    }

    public static void convol_pierre(final Img<UnsignedByteType> input,
                                     final Img<UnsignedByteType> output,
                                     int[][] kernel) {
        int size = kernel.length / 2;
        IntervalView<UnsignedByteType> expanded = Views.expandMirrorSingle(input, size, size, 0);
        Interval interval = Intervals.expand(expanded, -size, -size, 0);
        IntervalView<UnsignedByteType> intervalView = Views.interval(expanded, interval);
        IntervalView<UnsignedByteType> inputR = Views.hyperSlice(intervalView, 2, 0);
        IntervalView<UnsignedByteType> inputG = Views.hyperSlice(intervalView, 2, 1);
        IntervalView<UnsignedByteType> inputB = Views.hyperSlice(intervalView, 2, 2);

        var outputR = Views.hyperSlice(output, 2, 0).cursor();
        var outputG = Views.hyperSlice(output, 2, 1).cursor();
        var outputB = Views.hyperSlice(output, 2, 2).cursor();

        RectangleShape shape = new RectangleShape(size, false);
        var neighR = shape.neighborhoods(inputR);
        var neighG = shape.neighborhoods(inputG);
        var neighB = shape.neighborhoods(inputB);

        var neighsRCur = neighR.localizingCursor();
        var neighsGCur = neighG.localizingCursor();
        var neighsBCur = neighB.localizingCursor();

        var filterCoefficient = getFilterCoefficient(kernel);

        //convolution on R
        while (neighsRCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsRCur.fwd();
            neighsRCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsRCur.get();
            Cursor<UnsignedByteType> neighCur = neighborhood.localizingCursor();
            while (neighCur.hasNext()) {
                UnsignedByteType b = neighCur.next();
                int[] curPos = new int[2];
                neighCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputR.next().set(sum / filterCoefficient);
        }

        //convolution on G
        while (neighsGCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsGCur.fwd();
            neighsGCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsGCur.get();
            Cursor<UnsignedByteType> locCur = neighborhood.localizingCursor();
            while (locCur.hasNext()) {
                UnsignedByteType b = locCur.next();
                int[] curPos = new int[2];
                locCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputG.next().set(sum / filterCoefficient);
        }

        //convolution on B
        while (neighsBCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsBCur.fwd();
            neighsBCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsBCur.get();
            Cursor<UnsignedByteType> locCur = neighborhood.localizingCursor();
            while (locCur.hasNext()) {
                UnsignedByteType b = locCur.next();
                int[] curPos = new int[2];
                locCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputB.next().set(sum / filterCoefficient);
        }
    }

    public static void convolArgb(Img<ARGBType> input, Img<ARGBType> output, int[][] kernel) {
        int size = kernel.length / 2;
        IntervalView<ARGBType> expanded = Views.expandMirrorSingle(input, size, size);
        Interval interval = Intervals.expand(expanded, -size, -size);
        IntervalView<ARGBType> inputView = Views.interval(expanded, interval);
        Cursor<ARGBType> outCur = output.cursor();
        RectangleShape shape = new RectangleShape(size, false);
        int filterCoefficient = getFilterCoefficient(kernel);
        for (Neighborhood<ARGBType> neighborhood : shape.neighborhoods(inputView)) {
            int rSum = 0, gSum = 0, bSum = 0;
            int x = neighborhood.getIntPosition(0);
            int y = neighborhood.getIntPosition(1);
            Cursor<ARGBType> argbTypeCursor = neighborhood.localizingCursor();
            while (argbTypeCursor.hasNext()) {
                ARGBType argbVal = argbTypeCursor.next();
                int u = argbTypeCursor.getIntPosition(0) - x;
                int v = argbTypeCursor.getIntPosition(1) - y;
                int val = argbVal.get();
                int red = ARGBType.red(val);
                int green = ARGBType.green(val);
                int blue = ARGBType.blue(val);
                rSum += red * kernel[u + size][v + size];
                gSum += green * kernel[u + size][v + size];
                bSum += blue * kernel[u + size][v + size];
            }
            rSum /= filterCoefficient;
            gSum /= filterCoefficient;
            bSum /= filterCoefficient;
            outCur.next().set(ARGBType.rgba(rSum, gSum, bSum, 255));
        }
    }

    public static void convolutionFilterZero(final Img<UnsignedByteType> input,
                                             final Img<UnsignedByteType> output,
                                             int[][] kernel) {
        int size = kernel.length / 2;
        IntervalView<UnsignedByteType> expanded = Views.expandMirrorSingle(input, size, size, 0);
        Interval interval = Intervals.expand(expanded, -size, -size, 0);
        IntervalView<UnsignedByteType> intervalView = Views.interval(expanded, interval);
        IntervalView<UnsignedByteType> inputR = Views.hyperSlice(intervalView, 2, 0);
        IntervalView<UnsignedByteType> inputG = Views.hyperSlice(intervalView, 2, 1);
        IntervalView<UnsignedByteType> inputB = Views.hyperSlice(intervalView, 2, 2);

        var outputR = Views.hyperSlice(output, 2, 0).cursor();
        var outputG = Views.hyperSlice(output, 2, 1).cursor();
        var outputB = Views.hyperSlice(output, 2, 2).cursor();

        RectangleShape shape = new RectangleShape(size, false);
        var neighR = shape.neighborhoods(inputR);
        var neighG = shape.neighborhoods(inputG);
        var neighB = shape.neighborhoods(inputB);

        var neighsRCur = neighR.localizingCursor();
        var neighsGCur = neighG.localizingCursor();
        var neighsBCur = neighB.localizingCursor();

        int filterCoefficient = 1;

        //convolution on R
        while (neighsRCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsRCur.fwd();
            neighsRCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsRCur.get();
            Cursor<UnsignedByteType> neighCur = neighborhood.localizingCursor();
            while (neighCur.hasNext()) {
                UnsignedByteType b = neighCur.next();
                int[] curPos = new int[2];
                neighCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputR.next().set(sum / filterCoefficient);
        }

        //convolution on G
        while (neighsGCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsGCur.fwd();
            neighsGCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsGCur.get();
            Cursor<UnsignedByteType> locCur = neighborhood.localizingCursor();
            while (locCur.hasNext()) {
                UnsignedByteType b = locCur.next();
                int[] curPos = new int[2];
                locCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputG.next().set(sum / filterCoefficient);
        }

        //convolution on B
        while (neighsBCur.hasNext()) {
            int sum = 0;
            int[] pos = new int[2];
            neighsBCur.fwd();
            neighsBCur.localize(pos);
            Neighborhood<UnsignedByteType> neighborhood = neighsBCur.get();
            Cursor<UnsignedByteType> locCur = neighborhood.localizingCursor();
            while (locCur.hasNext()) {
                UnsignedByteType b = locCur.next();
                int[] curPos = new int[2];
                locCur.localize(curPos);
                int u = curPos[0] - pos[0];
                int v = curPos[1] - pos[1];
                sum += b.get() * kernel[u + size][v + size];
            }
            outputB.next().set(sum / filterCoefficient);
        }
    }

    /**
     * Apply a border detection filter
     *
     * @param input
     * @param output
     */
    public static void outlineDetectionFilter(final Img<UnsignedByteType> input,
                                              final Img<UnsignedByteType> output) {

        //initialize horizontal kernel matrix
        int[][] kernelHorizontal = new int[3][3];
        kernelHorizontal[0][0] = kernelHorizontal[2][0] = -1;
        kernelHorizontal[1][0] = -2;
        kernelHorizontal[0][1] = kernelHorizontal[1][1] = kernelHorizontal[2][1] = 0;
        kernelHorizontal[0][2] = kernelHorizontal[2][2] = 1;
        kernelHorizontal[1][2] = 2;

        //initialize vertical kernel matrix
        int[][] kernelVertical = new int[3][3];
        kernelVertical[0][0] = kernelVertical[0][2] = -1;
        kernelVertical[0][1] = -2;
        kernelVertical[1][0] = kernelVertical[1][1] = kernelVertical[1][2] = 0;
        kernelVertical[2][0] = kernelVertical[2][2] = 1;
        kernelVertical[2][1] = 2;

        // apply the convolution for horizontal
        Img<UnsignedByteType> outCopy = output.copy();
        convolutionFilterZero(input, output, kernelHorizontal);

        // apply the convolution for vertical
        convolutionFilterZero(input, outCopy, kernelVertical);

        // fusion outline convolution output
        imageFusion(output, outCopy);
    }

    private static void imageFusion(Img img1, Img img2) {
        Cursor<UnsignedByteType> cursor1 = img1.localizingCursor();
        Cursor<UnsignedByteType> cursor2 = img2.localizingCursor();

        while (cursor2.hasNext() && cursor1.hasNext()) {
            cursor1.fwd();
            cursor2.fwd();

            UnsignedByteType val1 = cursor1.get();
            UnsignedByteType val2 = cursor2.get();

            int value = val1.getInteger() + val2.getInteger();

            // in order to let the new value in the correct scale
            if (!(value > 255 || value < 0)) {
                val1.set(value);
            }
            if (value > 255) {
                val1.set(255);
            }
            if (value < 0) {
                val1.set(0);
            }

        }


    }

    private static int getFilterCoefficient(int[][] kernel) {
        int filterCoefficient = 0;
        for (int i = 0; i < kernel.length; i++) {
            for (int j = 0; j < kernel[0].length; j++) {
                filterCoefficient += kernel[i][j];
            }
        }
        return filterCoefficient;
    }

    public static void main(String[] args) throws ImgIOException, IncompatibleTypeException, InvocationTargetException, IllegalAccessException {

        // load image
        if (args.length < 2) {
            System.out.println("missing input or output image filename");
            System.exit(-1);
        }
        final String filename = args[0];
        final ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
        final ImgOpener imgOpener = new ImgOpener();
        final Img<UnsignedByteType> input = imgOpener.openImgs(filename, factory).get(0);
        imgOpener.context().dispose();

        // output image of same dimensions
        final Dimensions dim = input;
        final Img<UnsignedByteType> output = factory.create(dim);

        if (args.length > 2) {
            try {
                Method method = Arrays.stream(Convolution.class.getMethods())
                        .filter(m -> m.getName().equals(args[2]))
                        .findFirst()
                        .get();
                method.invoke(null, input, output);
            } catch (NoSuchElementException e) {
                System.err.println("No method with name " + args[2] + " was found.");
                return;
            }
        }

        final String outPath = args[1];
        File path = new File(outPath);
        if (path.exists()) {
            path.delete();
        }
        ImgSaver imgSaver = new ImgSaver();
        imgSaver.saveImg(outPath, output);
        imgSaver.context().dispose();
        System.out.println("Image saved in: " + outPath);
    }

    public static void meanConvolution(Img<UnsignedByteType> input, Img<UnsignedByteType> output, int filterSize) {
        //check parameter value
        if (filterSize != 3 && filterSize != 5 && filterSize != 7) {
            throw new IllegalArgumentException("Filter size is not correct");
        }

        int[][] kernel = new int[filterSize][filterSize];
        for (int i = 0; i < filterSize; i++) {
            for (int j = 0; j < filterSize; j++) {
                kernel[i][j] = 1;
            }
        }

        convol_pierre(input, output, kernel);
    }
}
