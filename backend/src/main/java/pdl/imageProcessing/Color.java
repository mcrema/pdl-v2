package pdl.imageProcessing;

import io.scif.img.ImgIOException;
import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import net.imglib2.Cursor;
import net.imglib2.Interval;
import net.imglib2.algorithm.neighborhood.Neighborhood;
import net.imglib2.algorithm.neighborhood.RectangleShape;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.loops.LoopBuilder;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.util.Intervals;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.NoSuchElementException;

public class Color {

    /**
     * to convert rgb values to hsv values.
     * h belongs to [0,360[ and s,v,r,g,b belong to [0,1]
     *
     * @param r   red level value
     * @param g   green level value
     * @param b   blue level value
     * @param hsv hsv's values array : 0=h, 1=s and 2=v
     */
    public static void rgbToHsv(int r, int g, int b, float[] hsv) {
        float t, s, v, max, min;

        // Compute of max
        max = Math.max(Math.max(r, g), b);
        // Compute of min
        min = Math.min(Math.min(r, g), b);

        // Compute of t
        if (min == max) {
            t = 0;
        } else if (max == r) {
            t = (60 * ((g - b) / (max - min)) + 360) % 360;
        } else if (max == g) {
            t = 60 * ((b - r) / (max - min)) + 120;
        } else {
            t = 60 * ((r - g) / (max - min)) + 240;
        }

        // Compute of s
        if (max == 0) {
            s = 0;
        } else {
            s = 1 - (min / max);
        }

        // Compute of v
        v = max / 255;

        hsv[0] = t;
        hsv[1] = s;
        hsv[2] = v;
    }

    /**
     * to convert hsv values to rgb values.
     * h belongs to [0,360[ and s,v,r,g,b belong to [0,1]
     *
     * @param h   hue value
     * @param s   saturation value
     * @param v   Value or brightness value
     * @param rgb rgb's array value : 0=r 1=g 2=b
     */
    public static void hsvToRgb(float h, float s, float v, int[] rgb) {
        // h belong to [0;360[ and r, g, and b are compute on [0;1[
        // but I don't make an exception in order to not complexify this code

        float hi, f, l, m, n;

        hi = (int) (h / 60) % 6;

        f = h / 60 - hi;
        l = v * (1 - s);
        m = v * (1 - f * s);
        n = v * (1 - (1 - f) * s);

        v *= 255;
        l *= 255;
        m *= 255;
        n *= 255;

        switch ((int) hi) {
            case 0:
                rgb[0] = (int) v;
                rgb[1] = (int) n;
                rgb[2] = (int) l;
                break;
            case 1:
                rgb[0] = (int) m;
                rgb[1] = (int) v;
                rgb[2] = (int) l;
                break;
            case 2:
                rgb[0] = (int) l;
                rgb[1] = (int) v;
                rgb[2] = (int) n;
                break;
            case 3:
                rgb[0] = (int) l;
                rgb[1] = (int) m;
                rgb[2] = (int) v;
                break;
            case 4:
                rgb[0] = (int) n;
                rgb[1] = (int) l;
                rgb[2] = (int) v;
                break;
            case 5:
                rgb[0] = (int) v;
                rgb[1] = (int) l;
                rgb[2] = (int) m;
                break;
        }
    }

    /**
     * filter to change h value in HSV model (so change hue value)
     *
     * @param input image which will be modified
     * @param newH  new hue value which belongs to [0, 360]
     */
    public static void filterColorHSVconvertion(Img<UnsignedByteType> input, float newH) {
        //check parameter value
        if(newH < 0 || newH > 359){
            throw new IllegalArgumentException("Parameter value is not in the correct bound [0, 359]");
        }

        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(input, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(input, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(input, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    float[] originalHSV = new float[3];
                    int[] rgb = new int[3];

                    // get the original h, s and v value
                    rgbToHsv(r.get(), g.get(), b.get(), originalHSV);
                    //compute the new rgb values
                    hsvToRgb(newH, originalHSV[1], originalHSV[2], rgb);

                    r.set(rgb[0]);
                    g.set(rgb[1]);
                    b.set(rgb[2]);
                }
        );
    }

    /**
     * To transform the input image to a negatif color version
     *
     * @param input ARGBType input imageF
     */
    public static void negatifFilter(Img<UnsignedByteType> input) {
        final IntervalView<UnsignedByteType> inputR = Views.hyperSlice(input, 2, 0);
        final IntervalView<UnsignedByteType> inputG = Views.hyperSlice(input, 2, 1);
        final IntervalView<UnsignedByteType> inputB = Views.hyperSlice(input, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {

                    int newValR = 255 - r.get();
                    int newValG = 255 - g.get();
                    int newValB = 255 - b.get();

                    r.set(newValR);
                    g.set(newValG);
                    b.set(newValB);
                }
        );
    }

    /**
     * to turn the image to a grey value image
     *
     * @param input input image
     */
    public static void fromColorToGrey(Img<UnsignedByteType> input) {
        IntervalView<UnsignedByteType> inputR = Views.hyperSlice(input, 2, 0);
        IntervalView<UnsignedByteType> inputG = Views.hyperSlice(input, 2, 1);
        IntervalView<UnsignedByteType> inputB = Views.hyperSlice(input, 2, 2);

        LoopBuilder.setImages(inputR, inputG, inputB).forEachPixel(
                (r, g, b) -> {
                    // 0.3R + 0.59G + 0.11B
                    int newR = (int) (r.get() * 0.3);
                    int newG = (int) (g.get() * 0.59);
                    int newB = (int) (b.get() * 0.11);
                    int newValue = newR + newG + newB;

                    r.set(newValue);
                    g.set(newValue);
                    b.set(newValue);
                }
        );

    }

    public static void main(String[] args) throws ImgIOException, IncompatibleTypeException, InvocationTargetException, IllegalAccessException {
        // load image
        if (args.length < 2) {
            System.out.println("missing input or output image filename");
            System.exit(-1);
        }
        final String filename = args[0];
        final ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
        final Img<UnsignedByteType> input = new ImgOpener().openImgs(filename, factory).get(0);


        // method(s) which compute filter (comment or uncomment to use the method)
        try {
            Method method = Arrays.stream(Color.class.getMethods())
                    .filter(m -> m.getName().equals(args[2]))
                    .findFirst()
                    .get();
            if (method.getParameterCount() > 1) {
                Class<?> secondParamType = method.getParameterTypes()[1];
                if (secondParamType.equals(float.class)) {
                    System.out.println("invoking " + method.getName());
                    method.invoke(null, input, Float.parseFloat(args[3]));
                } else if (secondParamType.equals(int.class)) {
                    System.out.println("invoking " + method.getName());
                    method.invoke(null, input, Integer.parseInt(args[3]));
                }
            } else {
                method.invoke(null, input);
            }
        } catch (NoSuchElementException e) {
            System.err.println("No method with name " + args[2] + " was found.");
            return;
        }

        // save output image
        final String outPath = args[1];
        File path = new File(outPath);
        if (path.exists()) {
            path.delete();
        }
        ImgSaver imgSaver = new ImgSaver();
        imgSaver.saveImg(outPath, input);
        System.out.println("Image saved in: " + outPath);
    }

}
