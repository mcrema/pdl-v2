# PDL Project

## Members of the group

* Mathias CREMA
* Maxime DUMONTEIL
* Pierre GRASSER

## Tested Operating Systems 

* Windows 10
* Pop!_OS 20.10 (based on Ubuntu 20.10)
* Debian 10 (Debian Buster)

## Tested web browsers

* Google Chrome (Windows 10 & Debian) : ✔
* Chromium (Pop OS) : ✔
* Mozilla Firefox (Windows 10 & Pop OS & Debian) : ✔
