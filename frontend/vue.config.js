module.exports = {
  outputDir: "target/dist",
  assetsDir: "static",
  devServer: {
    port: 8089,
    proxy: {
      "^/": {
        target: "http://localhost:8181", // Spring boot backend address
        ws: true,
        changeOrigin: true
      }
    }
  }
};
