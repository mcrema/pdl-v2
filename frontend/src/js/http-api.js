import axios from "axios";

import {client} from "@/js/websocket";


export default {
    callRestService() {
        console.log("callRestService");
        return axios.get(`images`)
            .then((response) => {
                // JSON responses are automatically parsed.
                this.response = response.data;
            })
            .catch((e) => {
                alert("error can't call rest service")
                this.errors.push(e);
            });
    },
    getAlgorithmsInfos() {
        console.log("get algorithms informations");
        return axios.get(`algorithms`)
            .then((response) => {
                // JSON responses are automatically parsed.
                this.algo_params_need = [];
                response.data.forEach(res => {
                    this.algo_names.push(res.name);
                    this.algo_descriptions.push(res.description);
                    this.algo_params_need.push(res.parameters);
                })
            })
            .catch((e) => {
                alert("Error get algorithms infos");
                this.errors.push(e);
            });
    },
    downloadImage(id) {
        console.log("downloadImage(" + id + ")");
        if (id === undefined || id < 0) return;
        return axios.get(`images\\` + id, {responseType: "blob"})
            .then(res => {
                let reader = new window.FileReader();
                reader.readAsDataURL(res.data);
                reader.onload = () => {
                    this.image = reader.result;
                    this.image_with_filter = undefined;
                    this.progress_status = 0;
                };
            });
    },
    downloadAllImages() {
        this.callRestService().then(() => {
            this.response.forEach(obj => {
                axios.get(`images\\` + obj.id, {responseType: "blob"})
                    .then(res => {
                        let reader = new window.FileReader();
                        reader.readAsDataURL(res.data);
                        reader.onload = () => {
                            this.images[obj.id] = reader.result;
                        };
                    })
                    .catch((e) => {
                        e.response.data.text().then(text => alert((JSON).parse(text).message));
                        this.errors.push(e);
                    })
            })
        })
    },
    handleFileUpload() {
        this.file = this.$refs.file.files[0];
    },
    submitFile() {
        let formData = new FormData();
        formData.append("file", this.file);
        return axios.post(`images`, formData, {headers: {"Content-Type": "multipart/form-data"}})
            .then(() => console.log("Submitted successfully."))
            .catch(() => console.log("Error submitting the file."));
    },
    submitFileAndUpdate() {
        this.submitFile().then(() => this.downloadAllImages());
    },
    downloadFile() {
        let formData = new FormData();
        formData.append("file", this.file);
        return axios.post(`images`, formData, {headers: {"Content-Type": "multipart/form-data"}})
            .then(() => console.log("Submitted successfully."))
            .catch(() => console.log("Error submitting the file."));
    },
    deleteImage(id) {
        console.log("deleteImage(" + id + ")");
        if (id === undefined || id < 0) return;
        return axios.delete(`images\\` + id)
            .then(() => this.images[id] = undefined)
            .then(() => {
                this.image = undefined;
                this.image_with_filter = undefined;
                this.progress_status = 0;
                this.selected = -1;
                console.log("Image deleted successfully");
            })
            .then(() => this.callRestService())
            .catch((e) => {
                e.response.data.text().then(text => alert((JSON).parse(text).message));
                this.errors.push(e);
                console.log("Error deleting the file.");
            });
    },
    updateGallery() {
        this.downloadAllImages();
    },
    selectImage(id) {
        if (this.$parent.selected !== id) {
            this.$parent.selected = id;
            this.$parent.downloadImage(this.$parent.selected);
        }
    },
    applyAlgos(id) {
        if (this.algo_list_to_call.length === 0 || this.algo_list_to_call.length === 1) {
            this.algo_list_to_call = [];
            this.addAlgoToList();
            if (this.algo_list_to_call.length === 0) {
                return;
            }
        }
        console.log("applyAlgos(" + id + ")");
        this.setFilteredImageName();
        return axios.post("images/" + id + "/process/add", JSON.stringify(this.algo_list_to_call), {
            headers: {"Content-Type": "application/json"},
            responseType: "json"
        })
            .then(res => {
                client.send(JSON.stringify(res.data));
            })
            .then(() => console.log("applyAlgo successfully"))
            .then(() => this.callRestService())
            .catch((e) => {
                e.response.data.text().then(text => alert((JSON).parse(text).message));
                this.errors.push(e);
                this.image_with_filter = undefined;
                this.progress_status = 0;
                console.log("Error apply algo");
            });
    },
    getProcessedImage() {
        console.log("getProcessedImage");
        return axios.get("images/" + this.selected + "/processed", {responseType: "blob"})
            .then(res => {
                let reader = new window.FileReader();
                reader.readAsDataURL(res.data);
                reader.onload = () => {
                    this.image_with_filter = reader.result;
                };
            });
    }
};