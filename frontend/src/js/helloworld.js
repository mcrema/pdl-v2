export default {
    downloadToLocalFiles(image, name) {
        let filePath = image;
        let link = document.createElement('a');
        link.href = filePath;
        link.download = name + filePath.substr(filePath.lastIndexOf('/'))
        link.click();
    },
    removeOneAlgoFromList(algo) {
        this.algo_list_to_call.splice(this.algo_list_to_call.indexOf(algo), 1);
    },
    removeAllAlgoFromList() {
        this.algo_list_to_call.splice(0);
    },
    addAlgoToList() {
        let algo_needs_param_nb = this.algo_params_need[this.selected_algo].length;

        // if the algo doesn't need paramater
        if (algo_needs_param_nb === 0) {
            // add selected algo to the list
            this.algo_list_to_call.push({'name': this.algo_names[this.selected_algo]});
            return;
        }
        // tab of the inputs values
        let inputVal = [];

        // loop on selected algo parameters input
        for (let i = 0; i < algo_needs_param_nb; i++) {
            let currentParam = this.algo_params_need[this.selected_algo][i];
            let input = document.getElementById(currentParam.name).value;
            if (input === "") {
                alert("Please enter a valid argument (number) in the interval [" + currentParam.bounds + ']');
                return;
            } else {
                input = Number.parseInt(input);
            }
            inputVal.push(input);
        }

        // add selected algo to the list (with parameters)
        this.algo_list_to_call.push({
            'name': this.algo_names[this.selected_algo],
            'parameters': inputVal
        });
    },

    setFilteredImageName() {
        this.filtered_image_name = "";
        for (let i = 0; i < this.algo_list_to_call.length; i++) {
            let current = this.algo_list_to_call[i];
            this.filtered_image_name += current.name + '_' + current.parameters + ' ';
        }
        this.filtered_image_name += "- " + this.response[this.selected].name;
    }
};