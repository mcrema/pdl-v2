import {w3cwebsocket} from "websocket";

export const client = new w3cwebsocket("ws://127.0.0.1:8887");

export default {

    initializeWebSocket() {
        client.onopen = () => console.log("websocket connected");
        client.onmessage = (message) => {
            let json = JSON.parse(message.data);
            this.progress_status = json.percentage;

            if (json.percentage === 100) {
                console.log("filter end");
                this.getProcessedImage();
            }
            if (json.error) {
                console.log(json.error);
            }
            console.log(message);
        }
    }

}
